// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Hopper_HollandHUD.generated.h"

UCLASS()
class AHopper_HollandHUD : public AHUD
{
	GENERATED_BODY()

public:
	AHopper_HollandHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

