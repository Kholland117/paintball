// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Hopper_HollandGameMode.generated.h"

UCLASS(minimalapi)
class AHopper_HollandGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHopper_HollandGameMode();
};



