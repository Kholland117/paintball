// Fill out your copyright notice in the Description page of Project Settings.

#include "Hopper_Holland.h"
#include "Hopper_HollandCharacter.h"
#include "AmmoCrate.h"


// Sets default values
AAmmoCrate::AAmmoCrate()	
{
	count = 30;
	TouchSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TouchSphereComponent"));
	TouchSphere->SetSphereRadius(45.f, false);
	TouchSphere->OnComponentBeginOverlap.AddDynamic(this, &AAmmoCrate::OnPickup);
	RootComponent = TouchSphere;

	thisMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	thisMesh->SetupAttachment(RootComponent);

}

void AAmmoCrate::OnPickup(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	AHopper_HollandCharacter *character = Cast<AHopper_HollandCharacter>(OtherActor);
	if (character)
	{
		character->ammoPool = character->ammoPool + count;
		this->Destroy();
	}
}
