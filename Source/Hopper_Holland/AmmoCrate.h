// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AmmoCrate.generated.h"

UCLASS()
class HOPPER_HOLLAND_API AAmmoCrate : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmoCrate();

	//Creating Variables for Reload Pickup

	UPROPERTY(VisibleAnywhere, Category = "Ammo Crate")
	class USphereComponent* TouchSphere;

	UPROPERTY(VisibleAnywhere, Category = "Ammo Crate")
	class UStaticMeshComponent* thisMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Crate")
	int32 count;

	UFUNCTION()
	void OnPickup(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);
};
