// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Hopper_HollandGameMode.h"
#include "Hopper_HollandHUD.h"
#include "Hopper_HollandCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHopper_HollandGameMode::AHopper_HollandGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHopper_HollandHUD::StaticClass();
}
